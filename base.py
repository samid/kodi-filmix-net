# -*- coding: utf-8 -*-
import urllib2, re, xbmc, xbmcgui, xbmcaddon, xbmcplugin, os, urllib, urllib2, socket, math, operator, base64, sys

try:
    import json
except ImportError:
    import simplejson as json

socket.setdefaulttimeout(12)

namespace = sys.argv[0]
handler = int(sys.argv[1])

icon = xbmc.translatePath('icon.png')
siteUrl = 'filmix.net'
baseUrl = 'http://' + siteUrl
httpSiteUrl = baseUrl + '/'
addon = xbmcaddon.Addon()

def add_menu_item(mode, name, page='1', isFolder=True):
    li = xbmcgui.ListItem(name)
    url = namespace + '?mode=' + mode + '&page=' + page
    #print(name+' '+mode)
    xbmcplugin.addDirectoryItem(handler, url, li, isFolder)

def add_any_menu_item(name, originalName, img, href, shortDesc, translate, rip, year, isFolder=True):
    showName = name
    if originalName:
        showName = showName + ' - ' + originalName
    if rip:
        showName = showName + ' - ' + rip
    if translate and not '<span>' in translate:
        showName = showName + ' - ' + translate
    if year:
        showName = showName + ' - ' + year
    li = xbmcgui.ListItem(uc(showName), iconImage=(baseUrl+img), thumbnailImage=(baseUrl+img))
    url = namespace + '?mode=OPEN_ANY&name=%s'%urllib.quote_plus(showName)
    url += '&url=%s'%urllib.quote_plus(href)
    url += '&img_url=%s'%urllib.quote_plus(img)
    #print('add_any '+showName)
    xbmcplugin.addDirectoryItem(handler, url, li, isFolder)

def add_play_menu_item(name, img, url):
    li = xbmcgui.ListItem(name, name, iconImage=(baseUrl+img), thumbnailImage=(baseUrl+img))
    li.setProperty('IsPlayable', 'true')
    xbmcplugin.addDirectoryItem(handler, url, li)

def set_end_of_menu():
    xbmcplugin.endOfDirectory(handler)

def showMessage(heading, message, times = 5000):
    xbmc.executebuiltin('XBMC.Notification(%s, %s, %s, %s)'%(heading, message, times, icon))

def get_params(paramstring):
    param=[]
    if len(paramstring)>=2:
        params=paramstring
        cleanedparams=params.replace('?','')
        if (params[len(params)-1]=='/'):
            params=params[0:len(params)-2]
        pairsofparams=cleanedparams.split('&')
        param={}
        for i in range(len(pairsofparams)):
            splitparams={}
            splitparams=pairsofparams[i].split('=')
            if (len(splitparams))==2:
                param[splitparams[0]]=splitparams[1]
    return param

def uc(str):
    return unicode(str, "cp1251")

def dec(param):
    hash1 = ("l","u","T","D","Q","H","0","3","G","1","f","M","p","U","a","I","6","k","d","s","b","W","5","e","y","=");
    hash2 = ("w","g","i","Z","c","R","z","v","x","n","N","2","8","J","X","t","9","V","7","4","B","m","Y","o","L","h");

    for i in range(0, len(hash1)):
        rr1 = hash1[i]
        rr2 = hash2[i]
        param = param.replace(rr1, '--')
        param = param.replace(rr2, rr1)
        param = param.replace('--', rr2)

    param = base64.b64decode(param)

    return param

def GET(url):
    try:
        print('def GET(%s):'%url)
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36')
        req.add_header('DNT', '1')
        f = urllib2.urlopen(req)
        a = f.read()
        f.close()
        return a
    except:
        showMessage('Не могу открыть URL def GET', url)
        return None

class BaseMenuFactory:
    mode = ''
    items = {}
    def __init__(self, params=[]):
        self.params = params

    def show_only_best_quality(self):
       return addon.getSetting('show_only_best_quality') == 'true'

    def build(self):
        for m, n in self.items.iteritems():
            add_menu_item(m, n)
        set_end_of_menu()


class OpenAnyMenuFactory(BaseMenuFactory):
    mode = 'OPEN_ANY'

    def build(self):
        xbmcplugin.setContent(handler, 'movies')
        url = urllib.unquote_plus(self.params['url'])
        name = urllib.unquote_plus(self.params['name'])
        img = urllib.unquote_plus(self.params['img_url'])

        httpWatch = GET(url)

        if httpWatch is not None:
            r0 = re.compile("videoLink = '(.*?)'", re.S).findall(httpWatch)
            if r0:
                if not '{video-link}' in r0[0]:
                    deccont = dec(r0[0])
                    print(deccont)
                    if deccont.endswith('.flv'):
                        add_play_menu_item(uc(name), img, deccont)
                    else:
                        r1 = re.compile("(.*?)\[(.*?)\](.*?)$", re.S).findall(deccont)
                        url = r1[0]
                        lst = url[1].split(',')
                        for v in lst:
                            if v:
                                add_play_menu_item(v + ' - ' + uc(name), img, url[0]+v+url[2])
                                if self.show_only_best_quality():
                                    break
                else:
                    r1 = re.compile("plLink = '(.*?)'", re.S).findall(httpWatch)
                    urld = dec(r1[0])

                    httpHls = GET(urld)

                    if httpHls is not None:
                        pl = dec(httpHls)
                        r2 = re.compile('{"playlist":(.*?)}$', re.S).findall(pl)

                        if len(r2) == 0:
                            showMessage('Показать нечего', 'Список пуст')
                            return False

                        print("list=%s"%r2[0])

                        items = json.loads(r2[0])

                        if len(items) == 0:
                            showMessage('Показать нечего', 'Список не найден')
                            return False

                        for item in items:
                            if item.has_key('playlist'):
                                for sub_item in item['playlist']:
                                    self.item_file(item['comment'] + ' - ' + sub_item['comment'], img, sub_item['file'], sub_item['isFlv'])
                            else:
                                self.item_file(item['comment'], img, item['file'], item['isFlv'])
            else:
                showMessage('Показать нечего', 'Нет видео')
        set_end_of_menu()

    def item_file(self, name, img, fileUrl, isFlv):
        print("fileUrl="+fileUrl)
        if isFlv == "1":
            print("flv-"+fileUrl)
            add_play_menu_item(name, img, fileUrl)
        else:
            print("fileUrl="+fileUrl)
            r1 = re.compile("(.*?)\[(.*?)\](.*?)$", re.S).findall(fileUrl)
            url = r1[0]
            lst = url[1].split(',')
            for v in lst:
                if v:
                    add_play_menu_item(v + ' - ' + name, img, url[0]+v+url[2])
                    if self.show_only_best_quality():
                        break


class FilmixMenuFactory(BaseMenuFactory):
    menu_type = ''

    def add_menu_item(self, name, originalName, img, href, shortDesc, translate, rip, year):
        add_any_menu_item(name, originalName, img, href, shortDesc, translate, rip, year)

    def build(self):
        xbmcplugin.setContent(handler, 'movies')
        page = self.params['page']
        print('current page ' + page)
        url = httpSiteUrl + self.menu_type + '/page/' + page
        http = GET(url)
        print("get done")
        if http is not None:
            print("http")
            content = re.compile('class="line-block">(.*?)</section>', re.S).findall(http)[0]
            #print("content = "+ content)
            rPrev = re.compile('<div class="navigation">.*?<a href="(.*?)" class="prev', re.S).findall(content)
            print("rPrev")
            rNext = re.compile('<div class="navigation">.*?</a>.*?<a href="(.*?)" class="next', re.S).findall(content)
            print("rNext")

            r1 = re.compile('data-pl-id="-1"(.*?)</article>', re.S).findall(content)
            print("r1 "+str(len(r1)))
            p = re.compile('.*?/.*?/.*?/.*?/.*?/(.*?)/')

            if len(rPrev) >0:
                page = '1'
                if rPrev[0] != httpSiteUrl + self.menu_type:
                    page = p.findall(rPrev[0])[0]
                print('prev '+page)
                add_menu_item("begin", 'Главное меню')
                add_menu_item(self.mode, '<< Предыдущая страница', page)

            for item in r1:
                r2 = re.compile('<div class="short">.*?href="(.*?)">.*?<img src="(.*?)".*?itemprop="name">(.*?)</div>.*?<div.*?line">(.*?)</div>(?:.*?item rip".*?added-info">)?(.*?)<.*?item year.*?added-info"><a.*?>(.*?)<.*?item translate.*?added-info">(.*?)</span>.*?<p*.?itemprop="description">(.*?)</p>', re.S).findall(item)
                #print("r2 "+str(len(r2[0])))
                if r2:
                    for href, img, name, originalName, rip, year, translate, shortDesc in r2:
                        self.add_menu_item(name, originalName, img, href, shortDesc, translate, rip, year)

            if len(rNext) >0:
                page = p.findall(rNext[0])[0]
                print('next '+page)
                add_menu_item(self.mode, '>> Следующая страница', page)

        set_end_of_menu()


class MultfilmyMenuFactory(FilmixMenuFactory):
    mode = 'multfilmy_sub'
    menu_type = 'multfilmy'

class TvMenuFactory(FilmixMenuFactory):
    mode = 'tv_sub'
    menu_type = 'tv'

class FilmsMenuFactory(FilmixMenuFactory):
    mode = 'films_sub'
    menu_type = 'films'

class SerialsMenuFactory(FilmixMenuFactory):
    mode = 'serials_sub'
    menu_type = 'serialy'


class SearchMenuFactory(BaseMenuFactory):
    mode = 'search'

    def build(self):
        xbmcplugin.setContent(handler, 'movies')
        skbd = xbmc.Keyboard()
        skbd.setHeading('Название фильма или часть названия')
        skbd.doModal()

        SearchString = skbd.getText(0)

        url = httpSiteUrl+'search/'+ SearchString

        http = GET(url)
        if http == None: return False

        content = re.compile('<div id="searchtable"(.*?)</section>.*?<aside', re.S).findall(http)[0]
        print('search content '+content)
        r1 = re.compile('<div class="short">.*?href="(.*?)">.*?<img src="(.*?)".*?itemprop="name">(.*?)</div>.*?<div.*?line">(.*?)<.*?item category".*?added-info">(.*?)<.*?item year.*?added-info"><a.*?>(.*?)<.*?item translate.*?added-info">(.*?)</span>.*?<p*.?itemprop="description">(.*?)</p>', re.S).findall(content)

        if len(r1) == 0:
            showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов')
            return False

        for href, img, name, originalName, category, year, translate, shortDesc in r1:
            add_any_menu_item(name, originalName, img, href, shortDesc, translate,"", year)

        set_end_of_menu()


class RootMenuFactory(BaseMenuFactory):
    mode = 'root'
    items = { 'films_sub': 'Фильмы',  'serials_sub': 'Сериалы', 'multfilmy_sub': 'Мультфильмы', 'tv_sub': 'Телепередачи','search': 'Поиск' }

    def build(self):
        try:
            mode = urllib.unquote_plus(self.params['mode'])
        except:
            BaseMenuFactory.build(self)
        else:
            factory = None
            if mode == "begin":
                xbmc.executebuiltin("XBMC.Container.Update(path,replace)")
                return True

            if mode == self.mode:
                BaseMenuFactory.build(self)
                return True

            if mode == FilmsMenuFactory.mode:
                factory = FilmsMenuFactory(self.params)

            if mode == SerialsMenuFactory.mode:
                factory = SerialsMenuFactory(self.params)

            if mode == MultfilmyMenuFactory.mode:
                factory = MultfilmyMenuFactory(self.params)

            if mode == TvMenuFactory.mode:
                factory = TvMenuFactory(self.params)

            if mode == SearchMenuFactory.mode:
                factory = SearchMenuFactory(self.params)

            if mode == OpenAnyMenuFactory.mode:
                factory = OpenAnyMenuFactory(self.params)

            #print(mode)

            factory.build()
